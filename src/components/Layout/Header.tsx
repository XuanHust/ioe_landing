'use client'

import { Navbar, NavbarBrand, NavbarContent, NavbarItem, NavbarMenu, NavbarMenuItem, NavbarMenuToggle } from "@nextui-org/react";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from 'next/navigation';
import NextNProgress from 'nextjs-progressbar';
import { useState } from "react";

const Header = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const pathname = usePathname()

  const menuItems = [
    "Trang chủ",
    "Về chúng tôi",
    "Sản phẩm",
    "Tuyển dụng",
    "Tin tức",
    "Liên hệ",
  ];

  return (<header>
    <div className="border-b-[1px] border-b-white fixed w-full top-0 left-0 z-50">
      <NextNProgress />
      <Navbar onMenuOpenChange={setIsMenuOpen} className="bg-gra479ef1">
        <NavbarContent>
          <NavbarMenuToggle
            aria-label={isMenuOpen ? "Close menu" : "Open menu"}
            className="sm:hidden"
          />
          <NavbarBrand>
            <Link className="font-bold text-inherit" href="/">
              <Image
                src="/images/nav-logo.svg"
                alt='FTECH-AI'
                sizes='100vw'
                width={140}
                height={110}
              />
            </Link>
          </NavbarBrand>
        </NavbarContent>

        <NavbarContent className="hidden sm:flex gap-4 text-white" justify="center">
          <NavbarItem isActive={pathname === '/'} className={`${pathname === '/' && 'active-btn-header'} hover:active-btn-header p-2`}>
            <Link color="foreground" href="/">
              Trang chủ
            </Link>
          </NavbarItem>
          <NavbarItem isActive={pathname === '/gioi-thieu'} className={`${pathname === '/gioi-thieu' && 'active-btn-header'} hover:active-btn-header p-2`}>
            <Link color="foreground" href="/gioi-thieu">
              Về chúng tôi
            </Link>
          </NavbarItem>
          <NavbarItem isActive={pathname === '/san-pham'} className={`${pathname === '/san-pham' && 'active-btn-header'} hover:active-btn-header p-2`}>
            <Link color="foreground" href="/san-pham">
              Sản phẩm
            </Link>
          </NavbarItem>
          <NavbarItem isActive={pathname === '/tuyen-dung'} className={`${pathname === '/tuyen-dung' && 'active-btn-header'} hover:active-btn-header p-2`}>
            <Link color="foreground" href="/tuyen-dung">
              Tuyển dụng
            </Link>
          </NavbarItem>
          <NavbarItem isActive={pathname === '/tin-tuc'} className={`${pathname === '/tin-tuc' && 'active-btn-header'} hover:active-btn-header p-2`}>
            <Link color="foreground" href="/tin-tuc">
              Tin tức
            </Link>
          </NavbarItem>
          <NavbarItem isActive={pathname === '/lien-he'} className={`${pathname === '/lien-he' && 'active-btn-header'} hover:active-btn-header p-2`}>
            <Link color="foreground" href="/lien-he">
              Liên hệ
            </Link>
          </NavbarItem>
        </NavbarContent>
        <NavbarMenu className="bg-[#f8f8f8]">
          {menuItems.map((item, index) => (
            <NavbarMenuItem key={`${item}-${index}`}>
              <Link
                // color={
                //   index === 2 ? "#2582ed" : index === menuItems.length - 1 ? "#e03e44" : "foreground"
                // }
                className={`w-full ${index === 2 ? 'text-[#2582ed]' : 'text-[#e03e44]'} hover:opacity-90`}
                href="#"
              >
                {item}
              </Link>
            </NavbarMenuItem>
          ))}
        </NavbarMenu>
      </Navbar>
    </div>
  </header>)
}
export default Header;