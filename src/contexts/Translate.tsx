'use client'

import { createContext, useContext, useMemo } from "react";
import enlocale from "../../public/locales/en.json";
import vilocale from "../../public/locales/vi.json";

interface ILocaleProviderContext {
  t: any;
}

export const LocaleContext = createContext({} as ILocaleProviderContext);

export const useLanguageContext = () => useContext(LocaleContext);

export const LanguageProvider = ({ children, locale }: any) => {
  const t = useMemo(() => (locale === "vi" ? vilocale : enlocale), [locale]);
  return (
    <LocaleContext.Provider value={{ t }}>{children}</LocaleContext.Provider>
  );
};
