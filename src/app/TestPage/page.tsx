import type { Metadata, ResolvingMetadata } from 'next'

type Props = {
  params: { slug: string }
  searchParams: { [key: string]: string | string[] | undefined }
}

export async function generateMetadata(
  { params, searchParams }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const id = params.slug

  // fetch data
  // const product = await fetch(`https://.../${id}`).then((res) => res.json())

  // optionally access and extend (rather than replace) parent metadata
  const previousImages = (await parent).openGraph?.images || []

  return {
    title: "Title",
    description: "description",
    openGraph: {
      title: "Title OR",
      description: "Mô tả",
      images: ['/some-specific-page-image.jpg', ...previousImages],
    },
  }
}

export default function TestPage({ params, searchParams }: Props) {
  return (
    <div>
      {/* local */}
      {/* <Image
        src=""
        alt=''
        sizes='100vw'
        style={{
          width: '100%',
          height: '100%'
        }}
      /> */}
      {/* remote : cần set relative cho cha
      <Image
        src=""
        alt=''
        quality={100}
        fill
        sizes='100vw'
        style={{
          objectFit: 'contain'
        }}
      /> */}
      Test
    </div>
  )
}