import { MetadataRoute } from 'next'

export default function manifest(): MetadataRoute.Manifest {
  return {
    "short_name": "Tiki",
    "name": "Tiki - Niềm vui mua sắm",
    "icons": [
      {
        "src": "96x96.png",
        "sizes": "96x96",
        "type": "image/png"
      },
      {
        "src": "144x144.png",
        "sizes": "144x144",
        "type": "image/png"
      },
      {
        "src": "192x192.png",
        "sizes": "192x192",
        "type": "image/png"
      }
    ],
    "theme_color": "#00b7f1",
    "background_color": "#00b7f1",
    "start_url": "/?homescreen=true&utm_source=homescreen&utm_medium=(none)",
    "display": "standalone",
    "orientation": "portrait",
  }
}