import qs from 'qs';


import serviceClient from './serviceClient';

export const serviceAPI = {
  getGlobalData(): Promise<any> {
    const query = qs.stringify({
      populate: {
        logo: '*',
        favicon: '*',
        socials: {
          populate: '*',
        },
        offices: {
          populate: '*',
        },
        footer: {
          populate: '*',
        },
        partners: {
          populate: '*',
        },
        defaultSeo: {
          populate: {
            metaTags: '*',
            metaImage: '*',
          },
        },
        event: {
          populate: {
            giftImage: '*',
            bgImage: '*',
          },
        },
      },
    });
    return serviceClient.get(`/api/global?${query}`);
  },
  getHomeToanTaiData() {
    const query = qs.stringify({
      populate: {
        defaultSeo: {
          populate: '*',
        },
        logo: '*',
        learnImage: '*',
        examImage: '*',
        challengeImage: '*',
        eventList: '*',
        targetList: '*',
        ruleList: '*',
        reviewList: {
          fields: ['nameStudent', 'schoolStudent', 'content', 'star', 'id'],
          populate: {
            image: '*',
          },
        },
        newsList: {
          populate: {
            articles: {
              populate: {
                category: '*',
                thumbImage: '*',
              },
              fields: ['title', 'slug', 'publishedAt', 'view'],
            },
          },
        },
      },
    });
    return serviceClient.get<any>(`/api/home-tt?${query}`);
  },
};
